# TestFoodItem-class-in-Java

public class TestFoodItem {
 
 public static void main(String [] args){
  FoodItem myCandy = new FoodItem("Snickers", 6.5, 0.55);
  FoodItem mySoup = new FoodItem("Progresso Minestrone", 16.5, 2.35);
  System.out.println(myCandy.toString());
  System.out.println("-- unit price is $" + myCandy.calcUnitPrice());
  System.out.println(mySoup.toString());
  System.out.println("-- unit price is $" + myCandy.calcUnitPrice());
 }
}  
